package com.techno207Proj.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "product")
public class ProductModel {
	@Id
	@Column(name = "id_product")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idproduct;
	@Column(name="nama")
	private String nama;
	@Column(name="brand")
	private String brand;
	@Column(name="madein")
	private String madein;
	@Column(name="price")
	private float price;
	
	  @Column(name="id_supplier")
	  private int idsupplier;
	
	  @ManyToOne
	  @JoinColumn(name="id_supplier", nullable=true, updatable=false,insertable=false) 
	  private SupplierModel splr;

	public int getIdproduct() {
		return idproduct;
	}

	public void setIdproduct(int idproduct) {
		this.idproduct = idproduct;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getMadein() {
		return madein;
	}

	public void setMadein(String madein) {
		this.madein = madein;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public int getIdsupplier() {
		return idsupplier;
	}

	public void setIdsupplier(int idsupplier) {
		this.idsupplier = idsupplier;
	}

	public SupplierModel getSplr() {
		return splr;
	}

	public void setSplr(SupplierModel splr) {
		this.splr = splr;
	}
	
	
	
}
