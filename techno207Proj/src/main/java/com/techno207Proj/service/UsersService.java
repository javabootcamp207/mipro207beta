package com.techno207Proj.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.techno207Proj.model.UsersModel;
import com.techno207Proj.repository.UsersRepository;

@Service
@Transactional
public class UsersService {
	
	@Autowired
	private UsersRepository usr;
	
	public List<UsersModel> listAll() {
		return usr.findAll(Sort.by("idusers").descending());
	}
	
	public UsersModel save(UsersModel UsersModel) {
		return usr.save(UsersModel);
	}
	
	public UsersModel get(int id_users) {
		return usr.findById(id_users).get();
	}
	
	public void delete(int id) {
		usr.deleteById(id);
	}
}
