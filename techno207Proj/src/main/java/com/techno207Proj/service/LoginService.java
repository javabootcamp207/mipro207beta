package com.techno207Proj.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.techno207Proj.repository.LoginRepository;

@Service
@Transactional
public class LoginService {

	@Autowired
	private LoginRepository lgn;
	
	public String ceklogin(String nama,String pass) {
		return lgn.cekLogin(nama,pass);
	}
	
}
