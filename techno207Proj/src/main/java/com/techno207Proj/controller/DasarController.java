package com.techno207Proj.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DasarController {

	@RequestMapping("/")
	public String awal(HttpSession session,Model model) {
		Object logses =  session.getAttribute("logsesUser");
		if (logses == null) {
			logses = new String();
		}
		String lg1 = logses.toString();
		
		String stsreturn="";
		if (lg1 != null && !lg1.isEmpty()) {
					model.addAttribute("user", lg1);
					stsreturn="awal";
				} else {
					stsreturn="redirect:/login";
				}
		return stsreturn;
	}
	
}
