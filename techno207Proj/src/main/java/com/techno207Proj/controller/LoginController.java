package com.techno207Proj.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.techno207Proj.service.LoginService;

@Controller
public class LoginController {
	
	@Autowired
	private LoginService lgn;
	
	@RequestMapping(value="/login")
	public String login(HttpSession session) {
		Object logses =  session.getAttribute("logsesUser");
		if (logses == null) {
			logses = new String();
		}
		String lg1 = logses.toString();
		System.out.println(lg1);
		String stsreturn="";
		if (lg1.equals("")) {
			stsreturn="/login";
		} else {
			stsreturn="redirect:/";
		}
		return stsreturn;
	}
	
	@RequestMapping(value="/signin" , method=RequestMethod.POST)
	public String login(@RequestParam(value="nama", required=false) String nama,
						@RequestParam(value="pass", required=false) String pass,
						HttpServletRequest request) {
		
		String stsreturn="test";
		String loglist=""; 
		loglist = lgn.ceklogin(nama, pass);// cekLogin(nama,pass).toString();
		
		if (loglist == "" || loglist == null) {
			//userpass salah
			stsreturn="redirect:/login";
		} else {
			//userpass benar
			request.getSession().setAttribute("logsesUser", nama);			
			stsreturn="redirect:/";
		}
		return stsreturn;
	}
	
	   //logout atau destroy
		@RequestMapping(value="/invalidate")
		public String destroySession(HttpServletRequest request) {
			request.getSession().invalidate();
			return "redirect:/login";
		}
	
}
