package com.techno207Proj.controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.techno207Proj.model.SupplierModel;
import com.techno207Proj.service.SupplierService;

@Controller
public class SupplierController {

	@Autowired
	private SupplierService spr;
	
	@RequestMapping("/supplier")
	public String supplier() {
		
		return "Supplier/supplier";
	}
	
	@RequestMapping("/splrlist")
	public String userlist(Model model) {
		List<SupplierModel> splrlist = spr.listAll();
		model.addAttribute("splrlist", splrlist);
		return "Supplier/supplierlist";
	}
	
	@RequestMapping("/splrnew")
	public String usersnew(Model model) {
		
		return "Supplier/suppliernew";
	}
	
	// simpan dan edit data
	@ResponseBody
	@RequestMapping(value = "/savesplr")
	public String savesplr(@ModelAttribute("splrModelDepan") SupplierModel SupplierModel) {
		String savests = "";
		//ambil nilai binding
		String emailmasuk=SupplierModel.getEmail();
		//cek email yang ada
		String cekemail=spr.CekEmailLama(emailmasuk);
		//cabang hasil
		if (cekemail==null) {
			savests="1";
			spr.save(SupplierModel);
		} else {
			savests="0";
		}
		
		return savests;
	}
	
	@RequestMapping("/splrdelfrm/{id}")
	public String deletefromsplr(@PathVariable(name = "id") int id, Model model) {
		SupplierModel SupplierModel = spr.get(id);
		model.addAttribute("splrmodeldepan", SupplierModel);
		return "Supplier/supplierdelet";
	}
	
	@ResponseBody
	@RequestMapping("/deletesplr/{id}")
	public String deletesplr(@PathVariable(name = "id") int id) {
		String delsts = "0";
		
			spr.delete(id);
	
		return delsts;
	}
	
	@RequestMapping("/splredit/{id}")
	public String editfromsplr(@PathVariable(name = "id") int id, Model model) {
		SupplierModel SupplierModel = spr.get(id);
		model.addAttribute("splrmodeldepan", SupplierModel);
		
		
		return "Supplier/supplieredit";
	}
	
}
