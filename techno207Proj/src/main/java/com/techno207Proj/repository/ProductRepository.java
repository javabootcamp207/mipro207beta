package com.techno207Proj.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.techno207Proj.model.ProductModel;
public interface ProductRepository extends JpaRepository<ProductModel,Integer>{
	
	//list semua product
	@Query(value = "SELECT p FROM ProductModel p ORDER BY idproduct DESC")
	List<ProductModel> ProdList();
	
	//list semua product dengan filter
	@Query(value = "SELECT p FROM ProductModel p where nama like %?1% or brand like %?1% ")
	List<ProductModel> ProdByName(String nama);
	
}
